package com.contedevel.netmodels;

/**
 * Created by ConteDevel on 4/20/15.
 *
 * @author Sologub Denis
 */
public class Log {

    public static void i(String msg) {
        System.out.print(msg);
    }

    public static void t(float[][] table) {
        int h = table[0].length;
        int w = table.length;

        for(int i = 0; i < w; ++i) {

            for(int j = 0; j < h; ++j) {
                i("\t\t" + table[i][j]);
            }

            i("\n");
        }
    }

    public static void t(int[][] table) {
        int h = table[0].length;
        int w = table.length;

        for(int i = 0; i < w; ++i) {

            for(int j = 0; j < h; ++j) {
                i("\t\t" + table[i][j]);
            }

            i("\n");
        }
    }

    public static void t(int[] table) {

        for(int i = 0; i < table.length; ++i) {
            i("\t\t" + table[i]);
        }
        i("\n");
    }

    public static void t(String[] table) {

        for(int i = 0; i < table.length; ++i) {
            i("\t\t" + table[i]);
        }
        i("\n");
    }
}
