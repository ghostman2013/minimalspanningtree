package com.contedevel.netmodels;

import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private Button bAdd;
    @FXML
    private Button bSave;
    @FXML
    private Button bClear;
    @FXML
    private MenuBar mbMainMenu;
    @FXML
    private RadioMenuItem rmiMatrix;
    @FXML
    private RadioMenuItem rmiList;

    @FXML
    private TableView<Edge> tvEdges;
    @FXML
    private TableView<ObservableList<StringProperty>> tvMatrix;

    private final ObservableList<Edge> edges = FXCollections.observableArrayList();

    private static final int VIEW_MATRIX = 0;
    private static final int VIEW_LIST = 1;

    private int view = VIEW_MATRIX;
    private SpanningTree tree = null;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        final ObservableList<TableColumn<Edge, ?>> columns = tvEdges.getColumns();
        TableColumn<Edge, String> cVertex1 = (TableColumn<Edge, String>) columns.get(0);
        cVertex1.setCellValueFactory(new PropertyValueFactory<Edge, String>("startVertex"));
        cVertex1.setCellFactory(TextFieldTableCell.<Edge>forTableColumn());
        cVertex1.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Edge, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Edge, String> t) {
                        t.getTableView().getItems().get(
                                t.getTablePosition().getRow()).setStartVertex(t.getNewValue());
                    }
                }
        );

        TableColumn<Edge, String> cVertex2 = (TableColumn<Edge, String>) columns.get(1);
        cVertex2.setCellValueFactory(new PropertyValueFactory<Edge, String>("endVertex"));
        cVertex2.setCellFactory(TextFieldTableCell.<Edge>forTableColumn());
        cVertex2.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Edge, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Edge, String> t) {
                        t.getTableView().getItems().get(
                                t.getTablePosition().getRow()).setEndVertex(t.getNewValue());
                    }
                }
        );

        TableColumn<Edge, Integer> cValue = (TableColumn<Edge, Integer>) columns.get(2);
        cValue.setCellValueFactory(new PropertyValueFactory<Edge, Integer>("value"));
        cValue.setCellFactory(TextFieldTableCell.<Edge, Integer>forTableColumn(new ProStringConverter()));
        cValue.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Edge, Integer>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Edge, Integer> t) {
                        t.getTableView().getItems().get(
                                t.getTablePosition().getRow()).setValue(t.getNewValue());
                    }
                }
        );

        tvEdges.setItems(edges);

        ToggleGroup toggleGroup = new ToggleGroup();
        rmiMatrix.setToggleGroup(toggleGroup);
        rmiList.setToggleGroup(toggleGroup);

        MenuItem miRemove = new MenuItem("Remove");
        miRemove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                int selectedIndex = tvEdges.getSelectionModel().getSelectedIndex();

                if(selectedIndex != -1) {
                    edges.remove(selectedIndex);
                }
            }
        });
        tvEdges.setContextMenu(new ContextMenu(miRemove));
    }

    @FXML
    private void buttonAddAction(ActionEvent event) {
        edges.add(new Edge("A", "A", 0));
    }

    @FXML
    private void buttonSaveAction(ActionEvent event) {
        List<SpanningTree.Node> nodes = convert(edges);

        if (nodes != null) {
            this.tree = new SpanningTree(nodes);
            updateView();
        }
    }

    @FXML
    private void buttonClearAction(ActionEvent event) {
        edges.clear();
    }

    @FXML
    private void menuOpenAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        File file = fileChooser.showOpenDialog(mbMainMenu.getScene().getWindow());

        if (file != null) {
            String content = Utils.readFile(file);
            Log.i("Content:\n");
            Log.i(content);
            Log.i("\n");
            String[] tokens = Utils.tokenizer(content.trim());
            Log.i("Tokens:\n");
            Log.t(tokens);
            Log.i("\n");
            List<SpanningTree.Node> nodes = Utils.buildNodes(tokens);

            edges.clear();

            for (SpanningTree.Node node : nodes) {
                Edge edge = convert(node);
                edges.add(edge);
            }
        }
    }

    @FXML
    private void menuSaveAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(mbMainMenu.getScene().getWindow());

        if(file != null){
            saveFile(file);
        }
    }

    private void saveFile(File file) {

        try {
            FileWriter fileWriter = new FileWriter(file);
            StringBuilder sb = new StringBuilder();

            for(Edge edge : edges) {
                sb = sb.append(edge.getStartVertex())
                        .append("\t").append(edge.getEndVertex())
                        .append("\t").append(edge.getValue()).append("\n");
            }

            fileWriter.write(sb.toString());

            fileWriter.close();
        } catch (IOException e) {
            Log.i("Cannot write data to file!\n");
            e.printStackTrace();
        }
    }

    @FXML
    private void menuSaveTableAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(mbMainMenu.getScene().getWindow());

        if(file != null){
            saveTableFile(file);
        }
    }

    private void saveTableFile(File file) {

        try {
            FileWriter fileWriter = new FileWriter(file);
            StringBuilder sb = new StringBuilder();

            ObservableList<TableColumn<ObservableList<StringProperty>, ?>> columns = tvMatrix.getColumns();

            for(TableColumn<ObservableList<StringProperty>, ?> column : columns) {
                sb.append("\t");
                sb.append(column.getText());
            }

            sb.append("\n");

            ObservableList<ObservableList<StringProperty>> items = tvMatrix.getItems();

            for(ObservableList<StringProperty> list : items) {

                for(StringProperty property : list) {
                    sb.append("\t");
                    sb.append(property.getValue());
                }

                sb.append("\n");
            }

            fileWriter.write(sb.toString());

            fileWriter.close();
        } catch (IOException e) {
            Log.i("Cannot write data to file!\n");
            e.printStackTrace();
        }
    }

    @FXML
    private void menuCloseAction(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    private void menuMatrixAction(ActionEvent event) {
        view = VIEW_MATRIX;
        updateView();
    }

    @FXML
    private void menuListAction(ActionEvent event) {
        view = VIEW_LIST;
        updateView();
    }

    private Edge convert(SpanningTree.Node node) {
        return new Edge(
                node.getStartVertex(),
                node.getEndVertex(),
                node.getValue()
        );
    }

    private void updateView() {
        clearView();

        if (tree != null) {

            if (view == VIEW_MATRIX) {
                makeMatrixView();
            }
            else {
                makeListView();
            }
        }
    }

    private void makeMatrixView() {
        String[] vertexes = tree.vertexes();
        int[][] optimalMatrix = tree.optimalMatrix();
        Log.i("Optimal matrix:\n");
        Log.t(optimalMatrix);

        int index = 0;
        tvMatrix.getColumns().add(createColumn(index, "V/V"));

        for (String vertex : vertexes) {
            ++index;
            tvMatrix.getColumns().add(createColumn(index, vertex));
        }

        for (int i = 0; i < vertexes.length; ++i) {
            ObservableList<StringProperty> data = FXCollections.observableArrayList();
            data.add(new SimpleStringProperty(vertexes[i]));

            for (int j = 0; j < optimalMatrix[0].length; ++j) {
                data.add(new SimpleStringProperty(String.valueOf(optimalMatrix[i][j])));
            }

            tvMatrix.getItems().add(data);
        }
    }

    private void makeListView() {
        String[] vertexes = tree.vertexes();
        int[][] optimalMatrix = tree.optimalMatrix();
        Log.i("Optimal matrix:\n");
        Log.t(optimalMatrix);

        tvMatrix.getColumns().add(createColumn(0, "Vertex 1"));
        tvMatrix.getColumns().add(createColumn(1, "Vertex 2"));
        tvMatrix.getColumns().add(createColumn(2, "Value"));

        for (int i = 0; i < vertexes.length; ++i) {

            for (int j = i; j < vertexes.length; ++j) {
                int value = optimalMatrix[i][j];

                if(value != 0) {
                    ObservableList<StringProperty> data = FXCollections.observableArrayList();
                    data.add(new SimpleStringProperty(vertexes[i]));
                    data.add(new SimpleStringProperty(vertexes[j]));
                    data.add(new SimpleStringProperty(String.valueOf(value)));
                    tvMatrix.getItems().add(data);
                }
            }
        }
    }

    private void clearView() {
        tvMatrix.getItems().clear();
        tvMatrix.getColumns().clear();
    }

    /**
     * Convert edge list to node list
     *
     * @param edges Edge list
     * @return Node list
     */
    private List<SpanningTree.Node> convert(ObservableList<Edge> edges) {
        if (edges == null)
            return null;

        List<SpanningTree.Node> nodes = new ArrayList<SpanningTree.Node>();

        for (Edge edge : edges) {
            SpanningTree.Node node = new SpanningTree.Node(
                    edge.getStartVertex(),
                    edge.getEndVertex(),
                    edge.getValue()
            );

            nodes.add(node);
        }

        return nodes;
    }

    private TableColumn<ObservableList<StringProperty>, String> createColumn(
            final int columnIndex,
            String columnTitle) {
        TableColumn<ObservableList<StringProperty>, String> column = new TableColumn<>();
        String title;
        if (columnTitle == null || columnTitle.trim().length() == 0) {
            title = "Column " + (columnIndex + 1);
        } else {
            title = columnTitle;
        }
        column.setText(title);
        column
                .setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList<StringProperty>, String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(
                            TableColumn.CellDataFeatures<ObservableList<StringProperty>, String> cellDataFeatures) {
                        ObservableList<StringProperty> values = cellDataFeatures.getValue();
                        if (columnIndex >= values.size()) {
                            return new SimpleStringProperty("");
                        } else {
                            return cellDataFeatures.getValue().get(columnIndex);
                        }
                    }
                });
        return column;
    }

    public static class Edge {
        private final SimpleStringProperty startVertex;
        private final SimpleStringProperty endVertex;
        private final SimpleIntegerProperty value;

        private Edge(String startVertex, String endVertex, int value) {
            this.startVertex = new SimpleStringProperty(startVertex);
            this.endVertex = new SimpleStringProperty(endVertex);
            this.value = new SimpleIntegerProperty(value);
        }

        public String getStartVertex() {
            return startVertex.get();
        }

        public SimpleStringProperty startVertexProperty() {
            return startVertex;
        }

        public void setStartVertex(String startVertex) {
            this.startVertex.set(startVertex);
        }

        public String getEndVertex() {
            return endVertex.get();
        }

        public SimpleStringProperty endVertexProperty() {
            return endVertex;
        }

        public void setEndVertex(String endVertex) {
            this.endVertex.set(endVertex);
        }

        public int getValue() {
            return value.get();
        }

        public SimpleIntegerProperty valueProperty() {
            return value;
        }

        public void setValue(int value) {
            this.value.set(value);
        }
    }

    public static class ProStringConverter extends StringConverter<Integer> {

        @Override
        public String toString(Integer integer) {
            return String.valueOf(integer);
        }

        @Override
        public Integer fromString(String s) {
            try {
                return Integer.parseInt(s);
            }
            catch (NumberFormatException e) {
                Log.i("Cannot convert to integer!\n");
                e.printStackTrace();
            }

            return 0;
        }
    }
}
