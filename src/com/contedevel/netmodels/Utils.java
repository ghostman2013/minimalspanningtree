package com.contedevel.netmodels;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ConteDevel on 5/19/15.
 *
 * @author Sologub Denis
 */
public final class Utils {
    private Utils() {
    }

    /**
     * Read text file into string
     *
     * @param file - File
     * @return - Text file content
     */
    public static String readFile(File file) {
        String content = null;

        try {
            FileReader fr = new FileReader(file.getPath()); //try open file
            BufferedReader br = new BufferedReader(fr);

            try {
                StringBuilder sb = new StringBuilder();

                String line;

                while ((line = br.readLine()) != null) { //read next line from text file
                    sb.append(line).append(System.lineSeparator());
                }

                br.close();
                content = sb.toString();
            } catch (IOException e) {
                Log.i("Error reading file!\n");
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            Log.i("File not found!\n");
        }

        return content;
    }

    /**
     * Get all tokens from string
     *
     * @param tokenList Token list
     * @return Tokens
     */
    public static String[] tokenizer(String tokenList) {
        return tokenList == null ? new String[0] : tokenList.split("\\s+");
    }

    /**
     * Builds node list by tokens
     *
     * @param tokens Token array
     * @return Node list
     */
    public static List<SpanningTree.Node> buildNodes(String[] tokens) {
        List<SpanningTree.Node> nodes = new ArrayList<SpanningTree.Node>();

        if (tokens != null) {
            final int length = tokens.length / 3;

            try {

                for (int i = 0; i < length; ++i) {
                    SpanningTree.Node node = new SpanningTree.Node();

                    node.setStartVertex(tokens[i * 3]);
                    node.setEndVertex(tokens[i * 3 + 1]);
                    String numericToken = tokens[i * 3 + 2];
                    node.setValue(Integer.parseInt(numericToken));

                    nodes.add(node);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.i("Incorrect token list!\n");
                e.printStackTrace();
            } catch (NumberFormatException e) {
                Log.i("Cannot parse numeric token!\n");
                e.printStackTrace();
            }
        }

        return nodes;
    }

    /**
     * Check if string is numeric type
     *
     * @param str Expected string
     * @return Check result
     */
    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}
