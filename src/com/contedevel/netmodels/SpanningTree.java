package com.contedevel.netmodels;

import java.util.List;

/**
 * Created by ConteDevel on 3/3/15.
 *
 * @author Sologub Denis
 */
public final class SpanningTree {
    private static final int X      = 0;
    private static final int Y      = 1;
    private static final int VALUE  = 2;

    private String[] vertexes;
    private int[][] matrix;

    /**
     * Constructor
     * @param treeList Tree edge list
     */
    public SpanningTree(String treeList) {
        vertexes = new String[0]; //Initialize empty vertex array
        String[] tokens = tokens(treeList);

        buildVertexes(tokens);

        int size = vertexes.length;
        matrix = new int[size][size];
        buildMatrix(tokens);
    }

    /**
     * Constructor
     * @param nodes List of nodes
     */
    public SpanningTree(List<Node> nodes) {
        vertexes = new String[0]; //Initialize empty vertex array
        buildVertexes(nodes);
        int size = vertexes.length;
        matrix = new int[size][size];
        buildMatrix(nodes);
    }

    /**
     * Build vertex list from input tokens
     * @param tokens Tokens
     */
    private void buildVertexes(String[] tokens) {

        for(String token : tokens) {

            if(!isNumeric(token) && isUniqueVertex(token)) {
                addVertex(token);
            }
        }
    }

    /**
     * Build vertex list from node list
     * @param nodes Tokens
     */
    private void buildVertexes(List<Node> nodes) {

        for(Node node : nodes) {

            if(isUniqueVertex(node.startVertex)) {
                addVertex(node.startVertex);
            }

            if(isUniqueVertex(node.endVertex)) {
                addVertex(node.endVertex);
            }
        }
    }

    /**
     * Build matrix from input tokens
     * @param tokens Tokens
     */
    private void buildMatrix(String[] tokens) {
        int length = tokens.length/3;

        for(int i = 0; i < length; ++i) {
            int vertical = vertexPosition(tokens[i * 3]);
            int horizontal = vertexPosition(tokens[i * 3 + 1]);
            int weight = Integer.parseInt(tokens[i * 3 + 2]);
            matrix[vertical][horizontal] = weight;
            matrix[horizontal][vertical] = weight;
        }
    }

    /**
     * Build matrix from node list
     * @param nodes Node list
     */
    private void buildMatrix(List<Node> nodes) {

        if(nodes == null)
            throw new NullPointerException("Cannot build matrix (node list is null)!");

        final int length = nodes.size();

        for(int i = 0; i < length; ++i) {
            Node node = nodes.get(i);
            int vertical = vertexPosition(node.startVertex);
            int horizontal = vertexPosition(node.endVertex);
            int weight = node.getValue();
            matrix[vertical][horizontal] = weight;
            matrix[horizontal][vertical] = weight;
        }
    }

    /**
     * Find optimal matrix
     * @return Optimal matrix (spanning tree's matrix)
     */
    public int[][] optimalMatrix() {
        int[][] optimalM = new int[matrix.length][matrix.length];
        boolean[] usedV = new boolean[vertexes.length];
        usedV[0] = true; //Select a first vertex to begin search

        while(true) {
            int[] data = findMinElement(usedV);
            int x = data[X];
            int y = data[Y];

            if(x == -1) //Check search result
                break;

            optimalM[x][y] = optimalM[y][x] = data[VALUE]; //Write value
            usedV[y] = true; //Mark vertex
        }

        return optimalM;
    }

    /**
     * Get vertex position by name
     * @param vertex Vertex name
     * @return Vertex position
     */
    public int vertexPosition(String vertex) {

        for(int i = 0; i < vertexes.length; ++i) {

            if(vertexes[i].equals(vertex)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Increment vertex list length
     * @param vertexes Vertexes
     */
    private void increaseVertexCount(String[] vertexes) {
        String[] tmpArray = new String[vertexes.length + 1];
        System.arraycopy(vertexes, 0, tmpArray, 0, vertexes.length);
        this.vertexes = tmpArray;
    }

    /**
     * Checks uniqueness of a vertex
     * @param vertex Expected vertex
     * @return Check result
     */
    private boolean isUniqueVertex(String vertex) {

        for(String v : vertexes) {

            if(v.equals(vertex))
                return false;

        }

        return true;
    }

    /**
     * Get all vertex names (sorted by their position in the matrix)
     * @return Array of vertex names
     */
    public String[] vertexes() {
        return vertexes;
    }

    /**
     * Get matrix
     * @return Built matrix
     */
    public int[][] matrix() {
        return matrix;
    }

    /**
     * Check if string is numeric type
     * @param str Expected string
     * @return Check result
     */
    private static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    /**
     * Add new vertex in vertex list
     * @param vertex
     */
    private void addVertex(String vertex) {
        increaseVertexCount(this.vertexes);
        vertexes[vertexes.length - 1] = vertex;
    }

    /**
     * Get all tokens from string
     * @param tokenList Token list
     * @return Tokens
     */
    private String[] tokens(String tokenList) {
        return tokenList == null ? new String[0] : tokenList.split("\\s+");
    }

    /**
     * Find minimal element in the matrix (last found)
     * @param usedV vertices
     * @return Minimal element and its coordinates
     */
    private int[] findMinElement(boolean[] usedV) {
        int[] data = new int[] {-1, -1, Integer.MAX_VALUE};

        for(int i = 0; i < usedV.length; ++i) {

            if(usedV[i]) { //Only for selected vertices

                for(int j = 0; j < usedV.length; ++j) {
                    int value = matrix[i][j];

                    if(!usedV[j] //Only for not selected vertices
                            && value < data[VALUE] && value != 0) {
                        data[VALUE] = value; //Write result
                        data[X] = i;
                        data[Y] = j;
                    }
                }
            }
        }

        return data;
    }

    public static class Node {
        private String startVertex;
        private String endVertex;
        private int value;

        public Node(String startVertex, String endVertex, int value) {
            this.startVertex = startVertex;
            this.endVertex = endVertex;
            this.value = value;
        }

        public Node() {
            this("A", "B", 0);
        }

        public String getStartVertex() {
            return startVertex;
        }

        public void setStartVertex(String startVertex) {
            this.startVertex = startVertex;
        }

        public String getEndVertex() {
            return endVertex;
        }

        public void setEndVertex(String endVertex) {
            this.endVertex = endVertex;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}
